import { Entity, BaseEntity, Column, ObjectIdColumn, ObjectID } from 'typeorm';
import uuidv4 from 'uuidv4';

@Entity()
export class Enquiry extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  date: Date;

  @Column()
  createdBy: string;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  phoneNumber: string;

  @Column()
  organizationType: string;

  @Column()
  team: string;

  @Column()
  queryType: string;

  @Column()
  priority: string;

  @Column()
  queryBrief: string;

  constructor() {
    super();
    if (!this.uuid) {
      this.uuid = uuidv4();
    }
  }
}
