import { Entity } from 'typeorm';
import { IsString, IsNotEmpty, IsEmail } from 'class-validator';

@Entity()
export class EnquiryDTO {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  phoneNumber: string;

  @IsString()
  @IsNotEmpty()
  organizationType: string;

  @IsString()
  @IsNotEmpty()
  team: string;

  @IsString()
  @IsNotEmpty()
  queryType: string;

  @IsString()
  @IsNotEmpty()
  priority: string;

  @IsString()
  @IsNotEmpty()
  queryBrief: string;
}
