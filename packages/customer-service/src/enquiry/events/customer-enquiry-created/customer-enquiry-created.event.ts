import { IEvent } from '@nestjs/cqrs';
import { Enquiry } from '../../entities/customer-enquiry/customer-enquiry.collection';

export class CustomerEnquiryCreatedEvent implements IEvent {
  constructor(public readonly provider: Enquiry) {}
}
